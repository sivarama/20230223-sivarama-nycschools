//
//  HighSchoolCell.swift
//  NYCSchools
//
//  Created by Siva Rama on 23/02/23.
//

import UIKit

class HighSchoolCell: UITableViewCell {

    @IBOutlet weak var schoolNameLabel: UILabel!

    func setData(_ model: HighSchool) {
        schoolNameLabel.text = model.schoolName
    }
}
