//
//  SchoolStatsViewModel.swift
//  NYCSchools
//
//  Created by Siva Rama on 23/02/23.
//

import Foundation
import RxSwift
import RxCocoa

class SchoolStatsViewModel {
    private(set) var schoolStats = BehaviorRelay<[SchoolStat]>(value: [])
    private(set) var error = BehaviorRelay<String>(value: "")
    private(set) var isLoading = BehaviorRelay<Bool>(value: false)
    private let disposeBag = DisposeBag()

    func fetchSchoolStats() {
        print("fetching high school statistics")
        let request = SchoolStatsRequest(path: "f9bf-2cp4.json")
        isLoading.accept(true)
        let single: Single<[SchoolStat]> = APIClient.send(request: request)
        single
            .observe(on: MainScheduler.instance)
            .subscribe { schoolStats in
                self.schoolStats.accept(schoolStats)
                self.isLoading.accept(false)
            } onFailure: { errorValue in
                self.isLoading.accept(false)
                self.error.accept(errorValue.localizedDescription)
            }
            .disposed(by: disposeBag)
    }
    
    func schoolStat(for school: HighSchool) -> SchoolStat? {
        schoolStats.value.filter { $0.dbn == school.dbn }.first
    }
}

struct SchoolStatsRequest: APIRequest {
    var method: HTTPMethod = .GET
    var path: String
    var parameters: [String : String]? = nil
    var body: [String : Any]? = nil
}
