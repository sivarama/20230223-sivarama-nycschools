//
//  HTTPHeaderField.swift
//  NYCSchools
//
//  Created by Siva Rama on 23/02/23.
//

import Foundation

enum HttpHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    
    public var value: String {
        return self.rawValue
    }
}

enum ContentType: String {
    case json = "application/json"
    
    public var value: String {
        return self.rawValue
    }
}

