//
//  EndPoints.swift
//  NYCSchools
//
//  Created by Siva Rama on 23/02/23.
//

import Foundation

struct EndPoint {
    static let baseURL = "https://data.cityofnewyork.us/resource"
}
