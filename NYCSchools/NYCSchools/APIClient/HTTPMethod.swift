//
//  HTTPMethod.swift
//  NYCSchools
//
//  Created by Siva Rama on 23/02/23.
//

import Foundation

enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case DELETE
    
    public var value: String {
        return self.rawValue
    }
}
