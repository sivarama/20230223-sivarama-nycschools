//
//  APIRequest.swift
//  NYCSchools
//
//  Created by Siva Rama on 23/02/23.
//

import Foundation

protocol APIRequest {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: [String: String]? { get }
    var body: [String: Any]? { get }
}

extension APIRequest {
    func request(with baseURLString: String = EndPoint.baseURL) -> URLRequest {
        guard let baseURL = try? baseURLString.asURL(),
              var components = URLComponents(url: baseURL.appendingPathComponent(path),
                                             resolvingAgainstBaseURL: false) else {
            fatalError("APIRequest.request(with \(baseURLString)): Unable to create URL components")
        }
        
        components.queryItems = parameters?.map {
            URLQueryItem(name: String($0), value: String($1))
        }
        
        guard let url = components.url else {
            fatalError("APIRequest.request(with \(baseURL)): Could not get url")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = String(describing: method)
        if method == .POST {
            let httpBody = try? JSONSerialization.data(withJSONObject: body!,
                                                       options: .prettyPrinted)
            request.httpBody = httpBody
        }
        
        request.addValue(ContentType.json.value,
                         forHTTPHeaderField: HttpHeaderField.contentType.value)
        request.addValue(ContentType.json.value,
                         forHTTPHeaderField: HttpHeaderField.acceptType.value)

        return request
    }
}
