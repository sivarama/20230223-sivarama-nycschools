//
//  ViewController.swift
//  NYCSchools
//
//  Created by Siva Rama on 23/02/23.
//

import UIKit
import RxSwift
import RxCocoa

class HighSchoolsScreen: UIViewController {

    @IBOutlet weak var schoolListTableView: UITableView!

    private var viewModel = HighSchoolViewModel()
    private var statsViewModel = SchoolStatsViewModel()
    private var selectedSchool: HighSchool?
    private var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        schoolListTableView.register(cellType: HighSchoolCell.self)
        schoolListTableView.rowHeight = UITableView.automaticDimension
        
        bindingUI()

        viewModel.fetchHighSchools()
        statsViewModel.fetchSchoolStats()
    }

    private func bindingUI() {
        viewModel.isLoading
            .bind(onNext: { [unowned self] loading in
                if loading { self.presentActivity() }
                else { self.dismissActivity() }
            })
            .disposed(by: disposeBag)
        viewModel.schools.asDriver()
            .drive(schoolListTableView.rx
                    .items(cellIdentifier: "HighSchoolCell",
                           cellType: HighSchoolCell.self)) {_, model, cell in
                cell.setData(model)
            }
            .disposed(by: disposeBag)
        schoolListTableView.rx.itemSelected.subscribe { [weak self] indexPath in
            guard let self = self else { return }
            if let path = indexPath.element {
                self.schoolListTableView.deselectRow(at: path, animated: true)
                self.selectedSchool = self.viewModel.schools.value[path.row]
                self.performSegue(withIdentifier: "SCHOOL_STATS", sender: self)
            }
        }.disposed(by: disposeBag)
        viewModel.error.asDriver()
            .drive(onNext: { [unowned self] error in
                if !error.isEmpty {
                    self.presentSimpleAlert(title: "Error", message: error)
                }
            })
            .disposed(by: disposeBag)
    }
}

extension HighSchoolsScreen {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let schoolStatsScreen = segue.destination as? SchoolStatsScreen {
            if let selectedSchool = selectedSchool,
                let stat = statsViewModel.schoolStat(for: selectedSchool) {
                schoolStatsScreen.schoolStat = stat
            } else {
                viewModel.noStatFound("No Stat found!")
            }
        }
    }
}

extension HighSchoolsScreen: ActivityPresentable {}
