//
//  SchoolStatsScreen.swift
//  NYCSchools
//
//  Created by Siva Rama on 24/02/23.
//

import UIKit
import RxCocoa
import RxSwift

class SchoolStatsScreen: UIViewController {

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!

    var schoolStat: SchoolStat?
    private var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        schoolNameLabel.text = schoolStat?.schoolName
        mathScoreLabel.text = "Math Score: " + (schoolStat?.mathScore ?? "")
        readingScoreLabel.text = "Reading Score: " + (schoolStat?.readingScore ?? "")
        writingLabel.text = "Writing Score: " + (schoolStat?.writingScore ?? "")
    }
}
