//
//  SchoolStat.swift
//  NYCSchools
//
//  Created by Siva Rama on 23/02/23.
//

import Foundation

struct SchoolStat: Codable {
    
    //MARK: - Properties
    let dbn: String
    let schoolName: String
    let readingScore: String
    let mathScore: String
    let writingScore: String

    //MARK: - Coding Keys
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
